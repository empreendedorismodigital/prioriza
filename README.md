# Prioriza
Aplicativo que auxilia pessoas com suas dívidas e que buscam entender sobre finanças pessoais.  
https://prioriza-me.herokuapp.com


## Como usar

``` bash
# Instalar as dependências do servidor (back-end)
npm install

# Instalar as dependências do cliente (front-end)
npm run client-install

# Instalar Material-UI
cd /client
npm install @material-ui/core

# Para rodar o servidor e o cliente juntos:
npm run dev

# Para rodar apenas o servidor:
npm run server

# Para rodar apenas o cliente:
npm run client

# OBS: Servidor roda no endereço http://localhost:5000, e o cliente em http://localhost:3000
```

## Informações:

Este aplicativo é um trabalho da disciplina Empreendedorismo Digital - FGV e IME, 2019.

### Integrantes

- Marcelo Baiano Pastorino Trylesinski
- Larissa Goto Sala
- Gabriely Rangel Pereira
- Victor Martins João
- Victoria Donzelli
- Guilherme Rodrigues Teles de Aguiar

