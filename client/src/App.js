import React, { Component } from "react";
import { Provider } from "react-redux";
import logo from "./logo.svg";
import "./App.css";
import store from "./store";
import Customers from "./components/Customer/customers";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Landing from "./pages/Landing/Landing";
import Debts from "./pages/Debts/Debts";
import Checkout from "./pages/DebtRegister/Checkout";
import Login from "./pages/Login/Login";
import Signup from "./pages/Signup/Signup";
import MyNavbar from "./pages/MyNavbar/MyNavbar";
import Home from './pages/Home/Home';
import ViewDebt from './pages/ViewDebt/ViewDebt';
import UserProfile from './pages/UserProfile/UserProfile';
import PayingDebts from './pages/PrioritizationPages/PayingDebts';
import PayableDebts from './pages/PrioritizationPages/PayableDebts';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <MyNavbar />
          <Switch>
            <Route path="/" exact={true} component={Landing} />
            <Route path="/dividas" component={Debts} />
            <Route path="/cadastro_divida" exact={true} component={Checkout} />
            <Route path="/login" exact={true} component={Login} />
            <Route path="/cadastro" exact={true} component={Signup} />
            <Route path="/home" exact={true} component={Home} />
            <Route path="/visualizar" exact={true} component={ViewDebt} />
            <Route path="/perfil" exact={true} component={UserProfile} />
            <Route path="/payable" exact={true} component={PayableDebts} />
            <Route path="/paying" exact={true} component={PayingDebts} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
