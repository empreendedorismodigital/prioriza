import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    height: 'auto',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: theme.palette.background.paper,
    justify: 'center',
    border: '1px solid #969696',
    boxShadow: theme.shadows[5],
    textAlign: 'center',
  },
  down: {
    marginTop: '10px',
  },
}));

const Alert = props => {
  const {title, description} = props;
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <div className={classes.down}>
        <a onClick={handleOpen} class="waves-effect btn-flat"><i class="material-icons">delete</i></a>
      </div>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.paper}>
          <h5 id="simple-modal-title">{title}</h5>
          <p id="simple-modal-description">
            {description}
          </p>
          <Grid container spacing={3}>
            <Grid item xs={6}><a onClick={props.handleYes} class="waves-effect btn-flat">Sim</a></Grid>
            <Grid item xs={6}><a onClick={handleClose} class="waves-effect btn-flat">Não</a></Grid>
          </Grid>
        </div>

      </Modal>
    </div>
  );
}

export default Alert;