import React from 'react'
import { Table } from 'react-materialize';
import Button from '@material-ui/core/Button';
import './DebtTable.css'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        marginTop: '40px',
        marginBottom: '10px',
        backgroundColor: '#F4AC06',
        '&$hover': {
          backgroundColor: '#808080',
        },
      },
      containedPrimary: {
        backgroundColor: '#F4AC06',
        '&:hover': {
          backgroundColor: '#808080',
        },
        '&:focus': {
          backgroundColor: '#808080',
        },
      },
    },
    MuiToolbar: {
      root: {
        backgroundColor: '#F4AC06',
      },
    },
  },
});

const DebtTable = props => {
  const { originalValue, installmentsNumber, installmentsValue,
          paymentDate, interestRate, finalValue, onClick } = props;
  return (
    <div className="center-align">
      <Table className="responsive-table">
        <thead>
          <tr> 
            <th> Valor original </th>
            <th> Qnt. de parcelas </th>
            <th> Valor da parcela </th>
            <th> Vencimento </th>
            <th> Taxa de juros </th>
            <th> Valor atual </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> R${originalValue ? originalValue.toFixed(2) : 0.00} </td>
            <td> {installmentsNumber} </td>
            <td> R${installmentsValue} </td>
            <td> {paymentDate} </td>
            <td> {interestRate}% </td>
            <td> R${finalValue ? finalValue.toFixed(2) : 0.00} </td>
          </tr>
        </tbody>
      </Table>
      <MuiThemeProvider theme={muiTheme}>
        <Button onClick={onClick}>
          Visualizar
        </Button>
      </MuiThemeProvider>
    </div>
  );
};

export default DebtTable;
