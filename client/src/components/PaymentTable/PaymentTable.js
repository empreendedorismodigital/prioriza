import React from 'react'
import { Table, Icon } from 'react-materialize';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, IconButton, Snackbar } from '@material-ui/core';

const useStyles = makeStyles({
  icon: {
      marginTop: '2%',
      color: '#969696',
  },
});

const PaymentTable = props => {
  var currentDate = new Date();
  currentDate = String(currentDate.getFullYear())+'-'+
                String(currentDate.getMonth()+1)+'-'+
                String(currentDate.getDate())
  const classes = useStyles();
  const { payments, hasNewPayment, parentCallback } = props;
  const [newField, setNewField] = React.useState(hasNewPayment);
  const [payment, setPayment] = React.useState({value: 0, date: currentDate});
  const [editable, setEditable] = React.useState(null);
  const [showAlert, setShowAlert] = React.useState(null);
  const [value, setValue] = React.useState(true);
  
  if (hasNewPayment != newField) {
    setNewField(hasNewPayment);
  }

  const savePayment = () => {
    if (payment.value > 0) {
      payments.push({value: payment.value, date: payment.date});
    }
    else {
      setShowAlert("Adicione um valor válido!");
    }
    setPayment({value: 0, date: currentDate});
  }

  const editPayment = index => {
    if (payment.value > 0) {
      payments[index] = {value: payment.value, date: payment.date};
    }
    else {
      setShowAlert("Adicione um valor válido!");
    }
    setPayment({value: 0, date: currentDate});
  }

  const deletePayment = index => {
    payments[index] = {value: 0, date: currentDate};
    setValue(!value);
  }

  const enableEdition = index => {
    if (!newField) {
      setPayment({value: payments[index].value, date: payments[index].date});
      setEditable(index);
    }
    else {
      setShowAlert("Finalize esta ediçao primeiro!");
    }
  }

  const disableEdition = () => {
    setEditable(null);
  }

  const hideAlert = () => {
    setShowAlert(null);
  };
  
  return (
    <div className="center-align">
      {showAlert != null
        ?<Snackbar
            onClose={hideAlert}
            open={true}
            message={showAlert}
          />
        : null
      }
      <Table className="centered">
        <thead>
          <tr> 
            <th> Valor pago </th>
            <th> Data </th>
            <th/>
            <th/>
          </tr>
        </thead>
        <tbody>
          {payments.length == 0
            ? <h6>Ainda nao há pagamentos</h6>
            : null}
          {payments.filter(function(x){return x.value != 0})
                   .map((data, i) => (
            <React.Fragment>
              {editable == i
              ? <tr>
                  <td>R$<TextField type='number'
                                  defaultValue={data.value}
                                  onChange={(e) => {
                                              setPayment({...payment, value: Number(e.target.value)});
                                            }}/>
                  </td>
                  <td><TextField type='date'
                                defaultValue={data.date}
                                onChange={(e) => {
                                            setPayment({...payment, date: e.target.value});
                                          }}/>
                  </td>
                  <td><IconButton
                        className={classes.icon}
                        size="small"
                        onClick={() => {
                          editPayment(i);
                          disableEdition();
                        }}>
                        <Icon>save</Icon>
                      </IconButton></td>
                  <td><IconButton
                    className={classes.icon}
                    size="small"
                    onClick={() => {
                      disableEdition();
                    }}>
                    <Icon>cancel</Icon>
                  </IconButton></td>
                </tr>
              : <tr>
                  <td> R${data.value} </td>
                  <td> {data.date} </td>
                  <td><IconButton
                        className={classes.icon}
                        size="small"
                        onClick={() => {
                          enableEdition(i);
                        }}>
                        <Icon>edit</Icon>
                      </IconButton></td>
                  <td><IconButton
                      className={classes.icon}
                      size="small"
                      onClick={() => {
                        deletePayment(i);
                      }}>
                      <Icon>delete</Icon>
                    </IconButton></td>
                </tr>
              }
            </React.Fragment>
          ))}
          {newField
            ? <tr>
                <td>R$<TextField type='number'
                                  onChange={(e) => {
                                              setPayment({...payment, value: Number(e.target.value)});
                                            }}/>
                  </td>
                <td><TextField type='date'
                               defaultValue={currentDate}
                               onChange={(e) => {
                                          setPayment({...payment, date: e.target.value});
                                        }}/>
                </td>
                <td><IconButton
                      className={classes.icon}
                      size="small"
                      onClick={() => {
                        savePayment();
                        parentCallback(false);
                        setNewField(false);
                      }}>
                      <Icon>save</Icon>
                    </IconButton></td>
                <td><IconButton
                      className={classes.icon}
                      size="small"
                      onClick={() => {
                        parentCallback(false);
                        setNewField(false);
                      }}>
                      <Icon>cancel</Icon>
                    </IconButton></td>
              </tr>
            : null
          }
        </tbody>
      </Table>
    </div>
  );
};
  
  export default PaymentTable;