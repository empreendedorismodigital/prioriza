import React from 'react'
import { Table, Button } from 'react-materialize';
import { makeStyles } from '@material-ui/core/styles';
import { Select, MenuItem, Icon } from '@material-ui/core';

const useStyles = makeStyles({
  icon: {
      color: 'green',
  },
  item: {
      width: '150px',
  }
});

const PaymentTableCheck = props => {
  var currentDate = new Date();
  currentDate = String(currentDate.getFullYear())+'-'+
                String(currentDate.getMonth()+1)+'-'+
                String(currentDate.getDate())
  const classes = useStyles();
  const { payments } = props;
  const [statePayments, setPayments] = React.useState(payments);

  const handleChange = index => event => {
    var tmp_payments = statePayments.slice();
    tmp_payments[index].status = event.target.value;
    setPayments(tmp_payments);
  };

  const updatePayments = () => {
    console.log("to update");
  }

  return (
    <div className="center-align">
      <Table className="centered">
        <thead>
          <tr> 
            <th> Valor </th>
            <th> Data </th>
            <th> Situação </th>
          </tr>
        </thead>
        <tbody>
          {statePayments.filter(function(x){return x.value != 0})
                   .map((data, i) => (
              <tr>
                <td> R${data.value} </td>
                <td> {data.date} </td>
                <td> <Select defaultValue={"aberto"}
                             className={classes.item}
                             value={data.status}
                             onChange={handleChange(i)}>
                            <MenuItem className={classes.item} value='aberto'><Icon color="primary">priority_high</Icon> Em aberto</MenuItem>
                            <MenuItem className={classes.item} value='pago'><Icon className={classes.icon}>done</Icon> Pago em dia</MenuItem>
                            <MenuItem className={classes.item} value='atrasado'><Icon color="secondary">close</Icon> Atrasado</MenuItem>
                     </Select>
                     {data.status == "atrasado"
                      ? <Button onClick={updatePayments}>Atualizar</Button>
                      : null}
                     </td>
              </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};
  
  export default PaymentTableCheck;