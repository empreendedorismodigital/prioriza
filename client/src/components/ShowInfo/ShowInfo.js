import React, { Component } from "react";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
	 flexGrow: 1,
   margin: 15,
  },
  paper: {
  	height: 115,
  	width: 250,
    textAlign: 'center',
  },
}));


const ShowInfo = props => {
  const {name1, value, name2, interest} = props;
  const classes = useStyles();

  return (
  	<Grid container justify="center" spacing={4} className={classes.root}>
  		<Grid item xs={12} sm={4}>
  			<Paper className={classes.paper}>
  			  <Grid className={classes.root}><Typography variant="h5">
  				  Dívida mais cara:
  			  </Typography></Grid>
  			  <Typography variant="h6">
  				  {name1}
  			  </Typography>
  			  R$ {value}
  			</Paper>
  		</Grid>
  		<Grid item xs={12} sm={4}>
  			<Paper className={classes.paper}>
  			  <Grid className={classes.root}><Typography variant="h5">
  				  Maior taxa de juros:
  			  </Typography></Grid>
  			  <Typography variant="h6">
  				  {name2}
  			  </Typography>
  			  {interest}% a.m.
  			</Paper>
  		</Grid>
  	</Grid>
  );
}

export default ShowInfo;