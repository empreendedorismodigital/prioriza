import React, {Component} from 'react';
import {render} from 'react-dom';
import {sortableContainer, sortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const SortableItem = sortableElement(({value}) => <li>{value}</li>);

const SortableContainer = sortableContainer(({children}) => {
  return <ul>{children}</ul>;
});


class Sortable extends Component {
  
  state = {
    items: this.props.name,
  };

  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState(({items}) => ({
      items: arrayMove(items, oldIndex, newIndex),
    }));
  };

  render() {
    const {items} = this.state;

    return (
      <Grid xs={7}>
        <Paper>
          <SortableContainer onSortEnd={this.onSortEnd}>
              {items.map((value, index) => (
                  <ListItem button>
                    <SortableItem key={`item-${value}`} index={index} value={value}/>
                  </ListItem>
              ))}
          </SortableContainer>
        </Paper>
      </Grid>
    );
  }
}

export default Sortable;