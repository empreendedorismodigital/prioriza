import React from 'react';
import { Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import { TextField, MenuItem, Select } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
      width: '100%',
      overflowX: 'auto',
    },
    table: {
      overflowX: 'auto',
    },
});

const ViewTable = props => {
    const classes = useStyles();
    const { values, columns, valueTypes, editable, fieldNames, onChange } = props;
    const [selected, setSelected] = React.useState(values[values.length-2]);
    

    console.log("values", values);
    
    return (
        <Table className={classes.table} aria-label="simple table">
            <TableBody>
                {columns.map((field, i) => (
                    <TableRow key={field}>
                        <TableCell align="left"> <b>{field}:</b> </TableCell>
                        <TableCell align="left">
                            { valueTypes[i] != 'select'
                              ? <TextField  value={values[i]}
                                            type={valueTypes[i]}
                                            multiline={valueTypes[i] == 'text'}
                                            rows={2}
                                            onChange={onChange(i)}
                                            disabled={!editable}/>

                              : <Select defaultValue={values[i]}
                                        value={values[i]}
                                        onChange={onChange(i)}
                                        disabled={!editable}
                                        >
                                        <MenuItem value='Composto'>Composto</MenuItem>
                                        <MenuItem value='Simples'>Simples</MenuItem>
                                </Select>
                            }
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    );
};
  
export default ViewTable;