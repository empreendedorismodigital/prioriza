import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import './DebtRegister.css';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
}));

const interest = [{type: 'Simples'}, {type: 'Composto'}]

export default function AdvancedInfo(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    type: 'Composto',
    interest:'0',
    fine:'0',
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  const onChange = (e) => {
    props.setDebtState(e);
  }

  const { installmentsNumber, installmentsValue, interestRate, interestType } = props.debt;
  
  return (
    <form className={classes.container} noValidate autoComplete="off">
      <React.Fragment>
        <Typography variant="h6" gutterBottom>
          Informações Avançadas
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <TextField 
              id="numParcela" 
              label="Quantas parcelas faltam ser pagas?" 
              type="number"
              helperText="Se não há parcelas, deixe o valor 0." 
              fullWidth
              name="installmentsNumber"
              value={installmentsNumber}
              onChange={onChange}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              id="numPaid" 
              label="Qual o valor da parcela?"
              InputProps={{
                startAdornment: <InputAdornment position="start">R$</InputAdornment>,
              }} 
              type="number" 
              helperText="Se não há parcelas, deixe o valor 0." 
              fullWidth 
              name="installmentsValue"
              value={installmentsValue}
              onChange={onChange}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              id="interest" 
              label="Qual a taxa de juros?"
              value={values.interest}
              onChange={handleChange('interest')}
              InputProps={{
                endAdornment: <InputAdornment position="end">% ao mês</InputAdornment>,
              }}
              helperText="Se o contrato não faz menção a juros, deixe o valor 0." 
              type="number" 
              fullWidth
              name="interestRate"
              value={interestRate}
              onChange={onChange}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              id="interestType"
              select
              label="Qual o tipo de juros?"
              className={classes.textField}
              value={values.type}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
              name="interestType"
              value={interestType}
              onChange={onChange}
            >
              {interest.map(option => (
                <MenuItem key={option.type} value={option.type}>
                  {option.type}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </React.Fragment>
    </form>
  );
}