import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import './DebtRegister.css';


export default function BasicInfo(props) {
  const onChange = (e) => {
    props.setDebtState(e);
  }

  const { name, description, initialValue, dueDate } = props.debt;
 
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Informações Básicas
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField 
            margin='none'
            required
            id="nameDebt"
            name="name"
            value={name}
            label="Dê um nome para a dívida"
            fullWidth
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="descriptionDebt"
            name="description"
            value={description}
            label="Descrição"
            multiline={true}
            rowsMax={4}
            fullWidth
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="debtValue"
            name="initialValue"
            value={initialValue}
            label="Quanto você está devendo?"
            InputProps={{
              startAdornment: <InputAdornment position="start">R$</InputAdornment>,
            }}
            type="number"
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            InputLabelProps={{ shrink: true }}
            required
            id="expireDebtDate"
            name="dueDate"
            type="date"
            value={dueDate}
            onChange={onChange}
            label="Qual a data de vencimento?"
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}