import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import BasicInfo from './BasicInfo';
import AdvancedInfo from './AdvancedInfo';
import Grid from '@material-ui/core/Grid';
import Review from './Review';
import './DebtRegister.css';
import { connect } from 'react-redux'
import { registerDebt } from '../../store/actions/debtActions';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Prioriza
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const muiTheme = createMuiTheme({
  overrides: {
    MuiStepIcon: {
      root: {
        color: '#808080',
        '&$active': {
          color: '#F4AC06',
        },
        '&$completed': {
          color: '#F4AC06',
        },
      },
    },
    MuiButton: {
      root: {
        backgroundColor: '#F4AC06',
        '&$hover': {
          backgroundColor: '#808080',
        },
      },
      containedPrimary: {
        backgroundColor: '#808080',
        '&:hover': {
          backgroundColor: '#F4AC06',
        },
        '&:focus': {
          backgroundColor: '#808080',
        },
      },
    },
    MuiButtonBase: {
      root: {
        backgroundColor: '#808080',
        '&:hover': {
          backgroundColor: '#F4AC06',
        },
      },
    },
  },
});

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const steps = ['Informações Básicas', 'Informações Avançadas', 'Revisão'];


function Checkout(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  
  const [state, setState] = useState({
    name: "",
    description: "",
    initialValue: 0,
    dueDate: "",
    installmentsNumber: 0,
    installmentsValue: 0,
    interestRate: 0,
    interestType: "Composto", 
    payments: []
  })

  const handleNext = () => {
    setActiveStep(activeStep + 1);
    if (activeStep == 1 && state.installmentsNumber > 1) {
      // add payments array to debt
      var payments = []
      for (var i = 0; i < state.installmentsNumber; i++) {
        var payment = { value: state.installmentsValue,
                        date: state.dueDate,
                        payed: false };
        payments.push(payment);
      }
      setState({...state, payments: payments})
    }
    if(activeStep + 1 == steps.length) {
      props.registerDebt(state);
    }
  };
  
  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const setDebtState = (e) => {
    setState({...state, [e.target.name]: e.target.value})
    console.log("state", state);
    console.log("e.target", e.target);
  }

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return <BasicInfo debt={state} setDebtState={setDebtState}/>;
      case 1:
        return <AdvancedInfo debt={state} setDebtState={setDebtState}/>;
      case 2:
        return <Review debt={state}/>;
      default:
        throw new Error('Unknown step');
    }
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Cadastre uma Dívida
          </Typography>
          <MuiThemeProvider theme={muiTheme}>
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
          </MuiThemeProvider>
          <React.Fragment>
            {activeStep === steps.length ? (
              <Grid align="center">
                <React.Fragment>
                  <Typography variant="h5" gutterBottom>
                    Dívida cadastrada!
                  </Typography>
                  <MuiThemeProvider theme={muiTheme}>
                    <Button href="/dividas">Voltar</Button>
                  </MuiThemeProvider>
                </React.Fragment>
              </Grid>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <div className={classes.buttons}>
                  {activeStep !== 0 && (
                    <MuiThemeProvider theme={muiTheme}>
                      <Button onClick={handleBack} className={classes.button}>
                        Voltar
                      </Button>
                    </MuiThemeProvider>
                  )}
                  <MuiThemeProvider theme={muiTheme}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={handleNext}
                      className={classes.button}
                    >
                      {activeStep === steps.length - 1 ? 'OK' : 'Avançar'}
                    </Button>
                  </MuiThemeProvider>
                </div>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
        <Copyright />
      </main>
    </React.Fragment>
  );
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = dispatch => {
  return { 
    registerDebt: (debtInfo) => dispatch(registerDebt(debtInfo))
  }
}


export default connect(null, mapDispatchToProps)(Checkout)