import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import "./DebtRegister.css";

// const data = ['Casas Bahia', 'Carnê da compra da geladeira', '200,00',
//              '30/10/2019', '10', '200,00', '10', 'Composto'];

const field = [
  "Nome da Dívida",
  "Descrição",
  "Valor atual da dívida",
  "Data de vencimento",
  "Quantidade de parcelas",
  "Valor da parcela",
  "Taxa de juros (% ao mês)",
  "Tipo de juros"
];

const useStyles = makeStyles(theme => ({
  listItem: {
    padding: theme.spacing(0.9, 0)
  },
  total: {
    fontWeight: "700"
  },
  title: {
    marginTop: theme.spacing(20)
  }
}));

export default function Review(props) {
  const classes = useStyles();

  const {
    name,
    description,
    initialValue,
    dueDate,
    installmentsNumber,
    installmentsValue,
    interestRate,
    interestType
  } = props.debt;

  const data = [
    name,
    description,
    initialValue,
    dueDate,
    installmentsNumber,
    installmentsValue,
    interestRate,
    interestType
  ];
  //   { name: "Nome da Dívida", value: name },
  //   { name: "Descrição", value: description },
  //   { name: "Valor original da dívida", value: initialValue },
  //   { name: "Data de início", value: dueDate },
  //   { name: "Quantidade de parcelas", value: installmentsNumber},
  //   { name: "Parcelas pagas", value: installmentsValue },
  //   { name: "Taxa de juros (% ao mês)", value: interestRate },
  //   { name: "Tipo de juros", value: interestType }
  //   // { name: 'Multa sobre a dívida (% ao mês)', value: '1,56' },
  // ];

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Está correto?
      </Typography>
      <List disablePadding>
        {data.map((data, i) => (
          <ListItem className={classes.listItem} key={i}>
            <ListItemText
              primary={field[i]}
              secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    variant="body2"
                    className={classes.inline}
                    color="textSecondary"
                  >
                    {data}
                  </Typography>
                </React.Fragment>
              }
            />
          </ListItem>
        ))}
      </List>
    </React.Fragment>
  );
}
