import React, { Component } from "react";
import { Row, Collapsible, CollapsibleItem } from "react-materialize";
import DebtTable from "../../components/DebtTable/DebtTable";
import "./Debts.css";
import { connect } from "react-redux";
import { loadDebts, selectDebt } from "../../store/actions/debtActions";
import Button from "@material-ui/core/Button";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ShowInfo from "../../components/ShowInfo/ShowInfo";

const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        marginTop: "40px",
        marginBottom: "10px",
        backgroundColor: "#d6d6d6",
        "&$hover": {
          backgroundColor: "#808080"
        }
      },
      containedPrimary: {
        backgroundColor: "#F4AC06",
        "&:hover": {
          backgroundColor: "#808080"
        },
        "&:focus": {
          backgroundColor: "#808080"
        }
      }
    }
  }
});

// const data = [
//   {
//     name: "Casas Bahia",
//     originalValue: "200,00",
//     installmentsNumber: "10",
//     installmentsValue:"200,00",
//     paymentDate: "30/10/2019",
//     interestRate: "10,0",
//     finalValue: "398,00"
//   },
//   {
//     name: "Renner",
//     originalValue: "279,90",
//     installmentsNumber: "0",
//     installmentsValue:"0,00",
//     paymentDate: "00/00/00",
//     interestRate: "0,0",
//     finalValue: "298,43"
//   },
//  {
//   name: "Pernambucanas",
//   originalValue: "79,90",
//   installmentsNumber: "0",
//   installmentsValue: "0,00",
//   paymentDate: "00/00/00",
//   interestRate: "0,0",
//   finalValue: "81,23"
//  },
//  {
//   name: "Marisa",
//   originalValue: "75,69",
//   installmentsNumber: "0",
//   installmentsValue: "0,00",
//   paymentDate: "00/00/00",
//   interestRate: "0,0",
//   finalValue: "77,58"
//  }
// ];

class Debts extends Component {
  componentDidMount() {
    this.props.loadDebts();
  }

  renderDebt = debt => {
    const {
      name,
      initialValue,
      interestRate,
      dueDate,
      installmentsNumber,
      installmentsValue,
      currentValue
    } = debt;
    return (
      <CollapsibleItem header={name} key={name}>
        <DebtTable
          name={name}
          originalValue={initialValue}
          installmentsNumber={installmentsNumber}
          installmentsValue={installmentsValue}
          paymentDate={dueDate}
          interestRate={interestRate}
          finalValue={currentValue}
          onClick={() => {
            console.log("on Click", debt);
            this.props.selectDebt(debt);
            this.props.history.push("/visualizar");
          }}
        ></DebtTable>
      </CollapsibleItem>
    );
  };

  renderDebtList = () => {
    return this.props.debts.map(debt => {
      return this.renderDebt(debt);
    });
  };

  render() {
    return (
      <Row className="container">
        <h2>Minhas Dívidas Ativas</h2>
        <ShowInfo
          name1={this.props.expensiveDebt.name}
          value={this.props.expensiveDebt.value ? this.props.expensiveDebt.value.toFixed(2) : 0.00}
          name2={this.props.highestInterestRate.name}
          interest={this.props.highestInterestRate.value}
        />
        <Collapsible className="popout">{this.renderDebtList()}</Collapsible>
        <Grid container justify="center" spacing={3}>
          <MuiThemeProvider theme={muiTheme}>
            <Grid item>
              <Button variant="contained" href="/paying">
                Ver dívidas em dia
              </Button>
            </Grid>
            <Grid item>
              <Button variant="contained" color="primary" href="/payable">
                Ver dívidas a serem pagas
              </Button>
            </Grid>
          </MuiThemeProvider>
        </Grid>
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  debts: state.debts.debts,
  highestInterestRate: state.debts.highestInterestRate,
  expensiveDebt: state.debts.expensiveDebt
});

const mapDispatchToProps = dispatch => {
  return {
    loadDebts: () => dispatch(loadDebts()),
    selectDebt: debt => dispatch(selectDebt(debt))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Debts);
