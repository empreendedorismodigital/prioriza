import React, { useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import { getDebtsTotalValue } from '../../store/actions/debtActions';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Prioriza
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

function createData(month, amount) {
    return { month, amount };
  }
  
const data = [
  createData('Mai', 800),
  createData('Jun', 1500),
  createData('Jul', 2000),
  createData('Ago', 2400),
  createData('Set', 3400),
  createData('Out', 3024),
];

const debt = ['3,024.00'];


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    textAlign: 'center',
  },
  fixedHeight: {
    height: 240,
  },
}));

const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        marginTop: '40px',
        marginBottom: '10px',
        backgroundColor: '#d6d6d6',
        '&$hover': {
          backgroundColor: '#808080',
        },
      },
      containedPrimary: {
        backgroundColor: '#F4AC06',
        '&:hover': {
          backgroundColor: '#808080',
        },
        '&:focus': {
          backgroundColor: '#808080',
        },
      },
    },
    MuiToolbar: {
      root: {
        backgroundColor: '#F4AC06',
      },
    },
  },
});

function Home(props) {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  useEffect(() => {
    const { totalValue, getDebtsTotalValue } = props

    if(!totalValue) {
      getDebtsTotalValue();
    }
  })

  return (
    <div className={classes.root}>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={8} lg={9}>
              <Paper className={fixedHeightPaper}>
                <React.Fragment>
                    <Title>Histórico de Dívidas</Title>
                    <ResponsiveContainer>
                        <LineChart
                        data={data}
                        margin={{
                            top: 16,
                            right: 16,
                            bottom: 0,
                            left: 24,
                        }}
                        >
                        <XAxis dataKey="month" />
                        <YAxis>
                            <Label angle={270} position="left" style={{ textAnchor: 'middle' }}>
                              R$
                            </Label>
                        </YAxis>
                        <Line type="monotone" dataKey="amount" stroke="#556CD6" dot={false} />
                        </LineChart>
                    </ResponsiveContainer>
                </React.Fragment>
              </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}>
                <React.Fragment>
                    <Title>Total de Dívidas atuais</Title>
                    <Typography component="p" variant="h4">
                        R${props.totalValue ? props.totalValue.toFixed(2) : 0.00}
                    </Typography>
                    <Box mt={2}>
                      <MuiThemeProvider theme={muiTheme}>
                        <Button href="/dividas">
                          Ver as dívidas ativas
                        </Button>
                      </MuiThemeProvider>
                    </Box>
                </React.Fragment>
              </Paper>
            </Grid>
            <Grid container justify="center" spacing={3}>
                <MuiThemeProvider theme={muiTheme}>
                  <Grid item xs={6} sm={2}>
                    <Button variant="contained" href="">
                      Ver histórico
                    </Button>
                  </Grid>
                  <Grid item xs={6} sm={2}>
                    <Button variant="contained" color="primary" href="/cadastro_divida">
                      Cadastrar dívida
                    </Button>
                  </Grid>
                </MuiThemeProvider>
            </Grid>
          </Grid>
        </Container>
        <Copyright />
      </main>
    </div>
  );
}

const mapStateToProps = (state) => ({
  totalValue: state.debts.totalValue
})

const mapDispatchToProps = dispatch => ({
  getDebtsTotalValue: () => dispatch(getDebtsTotalValue())
})


export default connect(mapStateToProps, mapDispatchToProps) (Home)