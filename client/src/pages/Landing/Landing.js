import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="/">
        Prioriza
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        backgroundColor: '#d6d6d6',
        '&$hover': {
          backgroundColor: '#808080',
        },
      },
      containedPrimary: {
        backgroundColor: '#F4AC06',
        '&:hover': {
          backgroundColor: '#808080',
        },
        '&:focus': {
          backgroundColor: '#808080',
        },
      },
    },
    MuiToolbar: {
      root: {
        backgroundColor: '#F4AC06',
      },
    },
  },
});

const useStyles = makeStyles(theme => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const cards = [{
  title: "Cadastre suas dívidas", 
  desc:"Coloque as informações de cada dívida para criarmos um sistema de priorização para você.", 
  img:"https://cdn1.vectorstock.com/i/1000x1000/86/25/coins-money-dollars-with-wallet-and-checklist-vector-26848625.jpg"
},
{
  title: "Veja suas dívidas",
  desc:"Tenha clareza de tudo o que você precisa pagar.",
  img:"https://cdn5.vectorstock.com/i/1000x1000/18/69/coins-money-dollars-with-checklist-and-bills-vector-26851869.jpg"
},
{
  title: "Saiba qual priorizar",
  desc:"Entenda os maiores gastos dessas dívidas e saiba como evitá-los.",
  img:"https://hcinvestimentos.com/wp-content/uploads/2009/06/Grafico-Juros-Simples-Juros-Compostos.png"
}];

export default function Landing() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <div className="center-align">
              <img src="prioriza_logo.png" width="160"/>
            </div>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Está endividado e precisa de ajuda com suas finanças pessoais?
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Prioriza está aqui para te ajudar a entender melhor para onde seu dinheiro está indo,
              e para onde deveria ir.
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <MuiThemeProvider theme={muiTheme}>
                    <Button variant="contained" color="primary" href="/cadastro">
                      Quero me cadastrar!
                    </Button>
                  </MuiThemeProvider>
                </Grid>
                <Grid item>
                  <MuiThemeProvider theme={muiTheme}>
                    <Button variant="outlined" href="/login">
                      Já possuo uma conta
                    </Button>
                  </MuiThemeProvider>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {cards.map(card => (
              <Grid item key={card} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={card.img}
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {card.title}
                    </Typography>
                    <Typography>
                      {card.desc}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          Prioriza
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Um aplicativo da disciplina Empreendedorismo Digital - FGV e IME
        </Typography>
        <Copyright />
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}