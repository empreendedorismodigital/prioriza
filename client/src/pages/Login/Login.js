import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux'

import "./Login.css";
import { authenticate } from '../../store/actions/authActions';
import { updateCurrentValue } from '../../store/actions/debtActions';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Prioriza
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: "#F0F1EC",
    },
    '.MuiButton-containedPrimary': {
      color: "#322F27",
      backgroundColor: "#f4ac06"
    },
    '.MuiInput-underline:after ': {
      borderBottomColor: "#f4ac06"
    },
    '.MuiButton-containedPrimary:hover': {
      backgroundColor: "#e2a006"
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    height: "100%",
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function Login(props) {
  const classes = useStyles();

  const [state, setState] = useState({
    email: "",
    password: ""
  })

  useEffect(() => {
    const { isAuthenticated } = props;
    
    if(isAuthenticated) {
      props.updateCurrentValue();
      props.history.push("/home");
    }
  })

  const onSubmit = (e) => {
    console.log("state", state);
    const { email, password } = state;
    
    console.log("email", email);
    console.log("password", password);
    console.log("props", props);
     
    e.preventDefault();
    props.authenticate({ email, password })
  }

  const onChange = (e) => {
    console.log("onChange", e.target.value);
    
    setState({...state, [e.target.name]: e.target.value});
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <img src="prioriza_logo.png" width="120"/>
        <form className={classes.form} noValidate onSubmit={onSubmit}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="E-mail"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={onChange}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Senha"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={onChange}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Entrar
          </Button>
          <Grid container style={{display: "flex", justifyContent: "center"}}>
            <Grid item style={{display: "flex", justifyContent: "center", marginTop: "8px", marginBottom: "8px" }}>
              <Link href="#" variant="body2" style={{textAlign: "center", color: "black"}}>
                {"Esqueci minha senha"}
              </Link>
            </Grid>
          </Grid>
          <Grid container style={{display: "flex", justifyContent: "center"}}>
            <Grid item style={{display: "flex", justifyContent: "center", marginTop: "8px", marginBottom: "8px"}}>
              <Link href="/cadastro" variant="body2" style={{textAlign: "center", color: "black"}}>
                {"Ainda não tenho cadastro e gostaria de me cadastrar!"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = dispatch => {
  return {
    authenticate: (info) => dispatch(authenticate(info)),
    updateCurrentValue: () => dispatch(updateCurrentValue())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);
