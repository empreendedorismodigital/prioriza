import React from 'react'
import { Navbar, NavItem, Dropdown, Icon } from 'react-materialize'
import './MyNavbar.css'

const MyNavbar = () => {
  return (
    <Navbar
      brand={<a href="/" className="pl">Prioriza</a>}
      alignLinks="right"
      sidenav={<ul><li><a className="sidenav-close"> <Icon>close</Icon> </a></li>
                   <li><a href="/" className="logo center-align"> <img src="prioriza_logo.png" width="100"/> </a></li>
                   <li><a href="/home"> Home </a></li>
                   <li><a href="/dividas"> Minhas Dívidas </a></li>
                   <li><a href="/cadastro_divida"> Nova Dívida </a></li>
                   <hr className="container"></hr>
                   <li><a href="/perfil"> Perfil </a></li>
                   <li><a href="/"> Sair </a></li></ul>}>
      <NavItem href="/home"> Home </NavItem>
      <NavItem href="/dividas"> Minhas Dívidas </NavItem>
      <NavItem href="/cadastro_divida"> Nova Dívida </NavItem>
      <Dropdown trigger={<a> Usuário <Icon right> arrow_drop_down </Icon></a>}>
        <a href="/perfil"> Perfil </a>
        <a href="/"> Sair </a>
      </Dropdown>
    </Navbar>
  );
};

export default MyNavbar;
