import React, { Component, useEffect } from "react";
import ShowInfo from "../../components/ShowInfo/ShowInfo";
import { Row } from "react-materialize";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Label,
  ResponsiveContainer,
  Tooltip,
  Legend
} from "recharts";
import Title from "../Home/Title";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { connect } from "react-redux";
import {
  loadPrioritizedDebts,
  selectDebt
} from "../../store/actions/debtActions";

function createData(month, Devedor, Ideal) {
  return { month, Devedor, Ideal };
}

const data = [
  createData(0, 398, 398),
  createData(1, 637.8, 598),
  createData(2, 901.58, 798),
  createData(3, 1191.74, 998),
  createData(4, 1510.91, 1198),
  createData(5, 1862, 1398)
];

function createDebt(priority, name, rate, date, value) {
  return { priority, name, rate, date, value };
}

const rows = [
  createDebt(1, "Casas Bahia", 10.0, "30/11/2019", 200),
  createDebt(2, "Magazine Luiza", 5.0, "26/12/2019", 190),
  createDebt(3, "Renner", 3.0, "14/11/2019", 40),
  createDebt(4, "Marisa", 4.2, "24/11/2019", 180)
];

const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        backgroundColor: "#F4AC06"
      }
    }
  }
});

const useStyles = makeStyles(theme => ({
  div: {
    textAlign: "center",
    paddingBottom: "4px",
    paddingTop: "4px"
  },
  paperBig: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    textAlign: "center",
    height: "400px"
  },
  center: {
    align: "center",
    left: "45%",
    right: "40%",
    marginTop: "10px",
    overflowX: "auto"
  },
  root: {
    width: "100%",
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  },
  btn: {
    width: "auto",
    fontSize: "10px"
  }
}));

function PayableDebts(props) {
  const classes = useStyles();

  useEffect(() => {
    const { prioritizedDebts, loadPrioritizedDebts } = props;

    if (!prioritizedDebts.length) {
      loadPrioritizedDebts();
    }
  });

  console.log("prioritizedDebts", props.prioritizedDebts);

  return (
    <Row className="container">
      <h2>Dívidas a serem pagas</h2>
      <Grid container justify="left" spacing={3}>
        <Grid item>Dívidas sem pagamentos associados e/ou atrasadas.</Grid>
      </Grid>
      <ShowInfo
        name1={props.prioritizedExpensiveDebt.name}
        value={props.prioritizedExpensiveDebt.value ? props.prioritizedExpensiveDebt.value.toFixed(2) : 0.00}
        name2={props.prioritizedHighestInterestRate.name}
        interest={props.prioritizedHighestInterestRate.value}
      />

      <div className={classes.div}>
        <Paper className={classes.paperBig}>
          <React.Fragment>
            <Title>E se eu não pagar nada pelos próximos meses?</Title>
            <ResponsiveContainer>
              <LineChart
                data={data}
                margin={{
                  top: 16,
                  right: 16,
                  bottom: 0,
                  left: 24
                }}
              >
                <Tooltip />
                <Legend />
                <XAxis dataKey="month" />
                <YAxis>
                  <Label
                    angle={270}
                    position="left"
                    style={{ textAnchor: "middle" }}
                  >
                    R$
                  </Label>
                </YAxis>
                <Line
                  type="monotone"
                  dataKey="Devedor"
                  stroke="#556CD6"
                  activeDot={{ r: 8 }}
                />
                <Line type="monotone" dataKey="Ideal" stroke="#F4AC06" />
              </LineChart>
            </ResponsiveContainer>
          </React.Fragment>
          <div className={classes.div}>
            <p>
              O saldo devedor é o valor da fatura (quantia que você vai ficar
              devendo naquele mês).
            </p>
          </div>
          <div className={classes.div}>
            A quantia ideal é o quanto você vai desembolsar se a partir de agora
            não atrasar mais nenhum mês.
          </div>
        </Paper>
      </div>
      <div className={classes.div}>
        <h2>Priorização recomendada:</h2>
      </div>

      <Paper className={classes.root}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">
                <b>Prioridade</b>
              </TableCell>
              <TableCell align="center">
                <b>Nome da Dívida</b>
              </TableCell>
              <TableCell align="center">
                <b>Taxa de juros (%a.m.)</b>
              </TableCell>
              <TableCell align="center">
                <b>Data de vencimento</b>
              </TableCell>
              <TableCell align="center">
                <b>Valor atual</b>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.prioritizedDebts.map((debt, index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row" align="center">
                  <b>{index + 1}</b>
                </TableCell>
                <TableCell align="center">{debt.name}</TableCell>
                <TableCell align="center">{debt.interestRate}</TableCell>
                <TableCell align="center">{debt.dueDate}</TableCell>
                <TableCell align="center">{debt.currentValue.toFixed(2)}</TableCell>
                <TableCell align="center">
                  <MuiThemeProvider theme={muiTheme}>
                    <Button
                      onClick={() => {
                        props.selectDebt(debt);
                        props.history.push("/visualizar");
                      }}
                      variant="contained"
                      className={classes.btn}
                    >
                      Visualizar
                    </Button>
                  </MuiThemeProvider>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>

      <MuiThemeProvider theme={muiTheme}>
        <Button className={classes.center} href="/dividas">
          Voltar
        </Button>
      </MuiThemeProvider>
    </Row>
  );
}

const mapStateToProps = state => ({
  prioritizedDebts: state.debts.prioritizedDebts,
  prioritizedHighestInterestRate: state.debts.prioritizedHighestInterestRate,
  prioritizedExpensiveDebt: state.debts.prioritizedExpensiveDebt
});

const mapDispatchToProps = dispatch => ({
  loadPrioritizedDebts: () => dispatch(loadPrioritizedDebts()),
  selectDebt: debt => dispatch(selectDebt(debt))
});

export default connect(mapStateToProps, mapDispatchToProps)(PayableDebts);
