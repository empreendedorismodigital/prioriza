import React, { Component } from "react";
import Grid from '@material-ui/core/Grid';
import { Row } from "react-materialize";
import Paper from '@material-ui/core/Paper';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {sortableContainer, sortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import Sortable from '../../components/Sortable/Sortable';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const muiTheme = createMuiTheme({
    overrides: {
      MuiButton: {
        root: {
          backgroundColor: '#F4AC06',
        },
      },
    },
});

const useStyles = makeStyles(theme => ({
    center: {
      align: 'center',
      left: '45%',
      right: '40%',
      marginTop: '10px',
      overflowX: 'auto',
    },
    root: {
      flexGrow: 1,
      margin: 15,
    },
    paper: {
      height: 115,
      width: 250,
      textAlign: 'center',
    },
    div: {
      textAlign: 'center',
      paddingBottom: '4px',
      paddingTop: '4px',
    },
}));

const names = ["Casas Bahia", "Marisa", "Riachuelo", "Ponto Frio"];

export default function PayingDebts() {

  const classes = useStyles();

  return (
    <Row className="container">
      <h2>Dívidas em dia</h2>
      <Grid container spacing={3}>
        <Grid item>
          Dívidas que não estão atrasadas.
        </Grid>
      </Grid>

      <Grid container justify="center" spacing={4} className={classes.root}>
        <Grid item xs={12} sm={4}>
          <Paper className={classes.paper}>
            <Grid className={classes.root}><Typography variant="h5">
              Você economizou
            </Typography></Grid>
            <Typography variant="h6">
              R$ 200,00
            </Typography>
          </Paper>
        </Grid>
      </Grid>

      <div className={classes.div}>
        Arraste para definir sua priorização:
      </div>

      <Grid container justify="center" className={classes.center}>
        <Sortable name={names}/>
      </Grid>

      <MuiThemeProvider theme={muiTheme}>
        <Button className={classes.center} href="/dividas">
          Voltar
        </Button>
      </MuiThemeProvider>

    </Row>


  );
}