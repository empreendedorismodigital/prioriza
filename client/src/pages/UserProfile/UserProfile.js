import React, { Component } from "react";
import { Container, Paper, Typography, Grid } from "@material-ui/core";
import {
  TextField,
  Icon,
  Divider,
  Button,
  InputAdornment
} from "@material-ui/core";
import "./UserProfile.css";
import { connect } from "react-redux";
import { loadUser, updateUserInfo } from "../../store/actions/authActions";

class UserProfile extends Component {
  constructor() {
    super();
    this.state = {
      edit: false,
      editButton: "button",
      submitButton: "button hide",
      name: "",
      email: "",
      cpf: "",
      surname: ""
    };
    this.enableEdition = this.enableEdition.bind(this);
    this.disableEdition = this.disableEdition.bind(this);
  }

  componentWillReceiveProps = (nextProps) => {
    console.log("nextProps", nextProps);
    console.log("this.props", this.props);
    
    if (this.props != nextProps) {
      if(nextProps.user){
        const { name, email, cpf } = nextProps.user;
        const nameSlip = name.split(" ");
        let surnames = "";
        nameSlip.forEach((surname, index) => {
          if (index) {
            if (index > 1) {
              surnames += " ";
            }
      
            surnames += surname;
          }
        });
        this.setState({
          name: nameSlip[0],
          surname: surnames,
          cpf,
          email
        });
      }
    }
  }
  
  componentDidMount() {
    this.props.loadUser();
  }

  enableEdition() {
    this.setState({
      edit: true,
      editButton: "button hide",
      submitButton: "button"
    });
  }

  disableEdition() {
    const { name, surname, cpf, email } = this.state;
    this.props.updateUserInfo({ name: name + " " + surname, cpf, email });
    this.setState({
      edit: false,
      editButton: "button",
      submitButton: "button hide"
    });
  }

  onChange = (state, event) => {
    this.setState({ [state]: event.target.value });
  };

  render() {
    const { name, surname, cpf, email } = this.state;
    return (
      <Container maxWidth="md">
        <Paper className="root">
          <Typography variant="h5" component="h3">
            Meu Perfil
          </Typography>
          <div className={this.state.editButton}>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.enableEdition}
            >
              Editar perfil
            </Button>
          </div>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <Icon>account_circle</Icon>
            </Grid>
            <Grid item>
              <TextField
                id="input-with-icon-grid"
                label="Nome"
                value={name}
                disabled={!this.state.edit}
                onChange={event => this.onChange("name", event)}
              />
            </Grid>
            <Grid item>
              <Icon></Icon>
            </Grid>
            <Grid item>
              <TextField
                id="input-with-icon-grid"
                label="Sobrenome"
                value={surname}
                onChange={event => this.onChange("surname", event)}
                disabled={!this.state.edit}
              />
            </Grid>
          </Grid>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <Icon>fingerprint</Icon>
            </Grid>
            <Grid item>
              <TextField
                id="input-with-icon-grid"
                label="CPF"
                value={cpf}
                onChange={event => this.onChange("cpf", event)}
                disabled={!this.state.edit}
              />
            </Grid>
          </Grid>
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <Icon>email</Icon>
            </Grid>
            <Grid item>
              <TextField
                id="input-with-icon-grid"
                label="Email"
                value={email}
                onChange={event => this.onChange("email", event)}
                disabled={!this.state.edit}
              />
            </Grid>
          </Grid>
          <Divider className="divider" variant="middle" />
          <Grid container spacing={1} alignItems="flex-end">
            <Grid item>
              <Icon>local_atm</Icon>
            </Grid>
            <Grid item>
              <TextField
                id="input-with-icon-grid"
                type="number"
                label="Valor disponível para pagar as dívidas"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">R$</InputAdornment>
                  )
                }}
                defaultValue="850"
                disabled={!this.state.edit}
              />
            </Grid>
          </Grid>
          <div className={this.state.submitButton}>
            <Button
              className="submit-button"
              variant="contained"
              color="primary"
              type="submit"
              onClick={this.disableEdition}
            >
              Salvar Alterações
            </Button>
          </div>
        </Paper>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  loadUser: () => dispatch(loadUser()),
  updateUserInfo: info => dispatch(updateUserInfo(info))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
