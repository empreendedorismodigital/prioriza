import React, { useEffect } from "react";
import { Row, Collapsible, CollapsibleItem, Icon } from "react-materialize";
import {
  makeStyles,
  MuiThemeProvider,
  createMuiTheme
} from "@material-ui/core/styles";
import { Grid, Paper, Button, Fab } from "@material-ui/core";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Label,
  ResponsiveContainer,
  Tooltip,
  Legend
} from "recharts";
import Title from "../Home/Title";
import ViewTable from "../../components/ViewTable/ViewTable";
import PaymentTable from "../../components/PaymentTable/PaymentTable";
import PaymentTableCheck from "../../components/PaymentTable/PaymentTableCheck";
import Alert from "../../components/Alert/Alert";
import { connect } from "react-redux";
import { deleteDebt, updateDebt } from "../../store/actions/debtActions";

const useStyles = makeStyles(theme => ({
  left: {
    width: "90%",
    position: "relative",
    align: "center",
    marginTop: "10px",
    overflowX: "auto"
  },
  center: {
    align: "center",
    left: "45%",
    right: "40%",
    marginTop: "10px",
    overflowX: "auto"
  },
  right: {
    marginLeft: "30%",
    color: "#969696"
  },
  down: {
    marginTop: "70%",
    color: "#969696"
  },
  div: {
    textAlign: "center",
    paddingBottom: "3px",
    paddingTop: "3px"
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    textAlign: "center",
    height: "300px"
  },
  paperBig: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    textAlign: "center",
    height: "400px"
  },
  fab: {
    display: "flex",
    justifyContent: "flex-end",
    padding: theme.spacing(1, 2)
  }
}));

const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        backgroundColor: "#F4AC06"
      }
    }
  }
});

// const debt = [
//   "Casas Bahia",
//   "Carnê da compra da geladeira",
//   200.0,
//   "2019-10-30",
//   10,
//   200.0,
//   10,
//   "Composto",
//   398.0
// ];

const fields = [
  "Nome da Dívida",
  "Descrição",
  "Valor inicial da dívida",
  "Data de vencimento",
  "Quantidade de parcelas",
  "Valor da parcela",
  "Taxa de juros (% ao mês)",
  "Tipo de juros"
];
const fieldTypes = [
  "text",
  "text",
  "number",
  "date",
  "number",
  "number",
  "number",
  "select",
  "number"
];

const fieldNames = [
  "name",
  "initialValue",
  "description",
  "installmentsNumber",
  "installmentsValue",
  "dueDate",
  "interestRate",
  "interestType"
];

const payments = [
  { value: 10.0, date: "2019-10-28", status: "aberto" },
  { value: 10.0, date: "2019-10-29", status: "aberto" }
];

function createData(month, Devedor, Ideal) {
  return { month, Devedor, Ideal };
}

const data = [
  createData(0, 398, 398),
  createData(1, 637.8, 598),
  createData(2, 901.58, 798),
  createData(3, 1191.74, 998),
  createData(4, 1510.91, 1198),
  createData(5, 1862, 1398)
];

const history = [
  {
    date: "27/10/2019",
    valor: 200.0
  },
  {
    date: "28/10/2019",
    valor: 190.0
  },
  {
    date: "29/10/2019",
    valor: 180.0
  },
  {
    date: "31/10/2019",
    valor: 398.0
  }
];

function sumPayments(payments) {
  return payments.reduce((sum, value) => sum + value.value, 0);
}

function ViewDebt(props) {
  const [state, setState] = React.useState({
    checked: false,
    debt: []
  });

  const [editable, setEditable] = React.useState(false);
  const [newPayment, setNewPayment] = React.useState(false);
  const [editButton, setEditButton] = React.useState("show");
  const [submitButton, setSubmitButton] = React.useState("hide");

  const handleChange = index => event => {
    const newDebt = [...state.debt];
    newDebt[index] = event.target.value;
    setState({ ...state, debt: newDebt });
  };
  const classes = useStyles();

  useEffect(() => {
    const { selectedDebt } = props;
    if (!state.debt.length && selectedDebt) {
      console.log("");

      const {
        name,
        initialValue,
        description,
        installmentsNumber,
        installmentsValue,
        dueDate,
        interestRate,
        interestType,
        currentValue,
        payments
      } = props.selectedDebt;

      setState({
        ...state,
        debt: [
          name,
          description,
          initialValue,
          dueDate,
          installmentsNumber,
          installmentsValue,
          interestRate,
          interestType,
          currentValue,
          payments
        ]
      });
    }
  });

  console.log("debts", state.debt);

  return (
    <Row className="container">
      <tr>
        <td>
          <h2>{state.debt.length ? state.debt[0] : ""}</h2>
        </td>
        <td>
          <Alert
            handleYes={() => {
              props.deleteDebt(props.selectedDebt._id);
              props.history.push("/dividas");
            }}
            description={
              "Tem certeza de que quer deletar esta dívida permanentemente?"
            }
          ></Alert>
        </td>
      </tr>

      <Collapsible className="popout">
        <CollapsibleItem
          header={
            <React.Fragment>
              <b>Informações Cadastradas</b>
            </React.Fragment>
          }
        >
          <div className={classes.fab}>
            <Fab
              color="primary"
              size="medium"
              variant="extended"
              aria-label="like"
              onClick={() => {
                setEditable(true);
                setEditButton("hide");
                setSubmitButton("show");
              }}
              className={editButton}
            >
              <Icon>edit</Icon> Editar
            </Fab>
          </div>
          <ViewTable
            values={state.debt}
            columns={fields}
            fieldNames={fieldNames}
            valueTypes={fieldTypes}
            editable={editable}
            onChange={handleChange}
          />
          <div className={classes.fab}>
            <Fab
              color="secondary"
              size="medium"
              variant="extended"
              aria-label="like"
              onClick={() => {
                console.log("cliquei em salvar");
                
                setEditable(false);
                setEditButton("show");
                setSubmitButton("hide");
                props.updateDebt(props.selectedDebt._id, state.debt)
              }}
              className={submitButton}
            >
              <Icon>save</Icon> Salvar
            </Fab>
          </div>
        </CollapsibleItem>
        <CollapsibleItem
          header={
            <React.Fragment>
              <b>Pagamentos</b>
            </React.Fragment>
          }
        >
          {state.debt[4] > 1 ? (
            <PaymentTableCheck payments={state.debt[9] || []} />
          ) : (
            <div>
              <div className={classes.fab}>
                <Fab
                  color="primary"
                  size="medium"
                  variant="extended"
                  aria-label="like"
                  onClick={() => {
                    setNewPayment(true);
                  }}
                  className={editButton}
                >
                  <Icon>add_box</Icon> Novo pagamento
                </Fab>
              </div>
              <PaymentTable
                payments={state.debt[9] || []}
                hasNewPayment={newPayment}
                parentCallback={setNewPayment}
              />
            </div>
          )}
          {newPayment}
        </CollapsibleItem>
      </Collapsible>

      <h2>Como está minha dívida?</h2>
      <Grid container spacing={3}>
        <Grid item xs={12} md={8} lg={9}>
          <div className={classes.div}>
            <Paper>
              <div className={classes.div}>
                <h6>Sua dívida atualmente vale</h6>
              </div>
              <div className={classes.div}>
                <h2>R$ {state.debt.length >= 9 && state.debt[8] ? state.debt[8].toFixed(2) : ""}</h2>
              </div>
            </Paper>
          </div>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <div className={classes.div}>
            <Paper>
              <div className={classes.div}>
                <h6>Quantia paga: </h6>
              </div>
              <div className={classes.div}>
                <h2>R$ {sumPayments(payments)}</h2>
              </div>
            </Paper>
          </div>
        </Grid>
      </Grid>
      <div className={classes.div}>
        <Paper className={classes.paper}>
          <React.Fragment>
            <Title>Andamento da dívida</Title>
            <ResponsiveContainer>
              <LineChart
                data={history}
                margin={{
                  top: 16,
                  right: 16,
                  bottom: 0,
                  left: 24
                }}
              >
                <Tooltip />
                <Legend />
                <XAxis dataKey="date" hide="true" />
                <YAxis>
                  <Label
                    angle={270}
                    position="left"
                    style={{ textAnchor: "middle" }}
                  >
                    R$
                  </Label>
                </YAxis>
                <Line
                  type="monotone"
                  dataKey="valor"
                  stroke="#556CD6"
                  activeDot={{ r: 8 }}
                />
              </LineChart>
            </ResponsiveContainer>
          </React.Fragment>
        </Paper>
      </div>
      <div className={classes.div}>
        <Paper className={classes.paperBig}>
          <React.Fragment>
            <Title>E se eu não pagar nada pelos próximos meses?</Title>
            <ResponsiveContainer>
              <LineChart
                data={data}
                margin={{
                  top: 16,
                  right: 16,
                  bottom: 0,
                  left: 24
                }}
              >
                <Tooltip />
                <Legend />
                <XAxis dataKey="month" />
                <YAxis>
                  <Label
                    angle={270}
                    position="left"
                    style={{ textAnchor: "middle" }}
                  >
                    R$
                  </Label>
                </YAxis>
                <Line
                  type="monotone"
                  dataKey="Devedor"
                  stroke="#556CD6"
                  activeDot={{ r: 8 }}
                />
                <Line type="monotone" dataKey="Ideal" stroke="#82ca9d" />
              </LineChart>
            </ResponsiveContainer>
          </React.Fragment>
          <div className={classes.div}>
            <p>
              O saldo devedor é o valor da fatura (quantia que você vai ficar
              devendo naquele mês).
            </p>
          </div>
          <div className={classes.div}>
            A quantia ideal é o quanto você vai desembolsar se a partir de agora
            não atrasar mais nenhum mês.
          </div>
        </Paper>
      </div>
      <MuiThemeProvider theme={muiTheme}>
        <Button className={classes.center} href="/dividas">
          Voltar
        </Button>
      </MuiThemeProvider>
    </Row>
  );
}

const mapStateToProps = state => ({
  selectedDebt: state.debts.selectedDebt
});

const mapDispatchToProps = dispatch => ({
  deleteDebt: debtId => dispatch(deleteDebt(debtId)),
  updateDebt: (debtId, updatedDebt) => dispatch(updateDebt(debtId, updatedDebt))
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewDebt);
