import {
  USER_LOADING,
  USER_LOADED,
  AUTH_ERROR,
  AUTH_LOADING,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_UPDATED
} from "./constants";
import { returnErrors } from "./errorActions";
import axios from "axios";

// Check token & load user
export const loadUser = () => {
  return (dispatch, getState) => {
    // User loading
    dispatch({ type: USER_LOADING });

    axios
      .get("/api/auth/user", tokenConfig(getState))
      .then(res => dispatch({ type: USER_LOADED, payload: res.data.user }))
      .catch(err => {
        dispatch({
          type: AUTH_ERROR
        });
      });
  };
};

export const updateUserInfo = ({ email, cpf, name }) => {
  return (dispatch, getState) => {
    const body = JSON.stringify({ email, cpf, name });

    axios
      .put("/api/user", body, tokenConfig(getState))
      .then(res => dispatch({ type: USER_UPDATED, payload: res.data.user }))
      .catch(err => {
        console.log("err", err);
        
      });
  };
};

// Authenticate
export const authenticate = ({ email, password }) => {
  return dispatch => {
    dispatch({ type: AUTH_LOADING });

    const config = headerConfig();

    const body = JSON.stringify({ email, password });

    axios
      .post("/api/auth", body, config)
      .then(res => dispatch({ type: LOGIN_SUCCESS, payload: { ...res.data } }))
      .catch(err => {
        const { data, status } = err.response;
        dispatch(returnErrors(data, status));
        dispatch({
          type: AUTH_ERROR
        });
      });
  };
};

// Register
export const register = ({ email, password, cpf, name }) => {
  return dispatch => {
    const config = headerConfig();

    const body = JSON.stringify({ email, password, cpf, name });

    axios
      .post("/api/user", body, config)
      .then(res => {
        console.log("res register", res);

        dispatch({ type: REGISTER_SUCCESS, payload: { ...res.data } });
      })
      .catch(err => {
        console.log("erro register", err);

        dispatch({ type: REGISTER_FAIL });
      });
  };
};

export const headerConfig = () => {
  const config = {
    headers: {
      "Content-type": "application/json"
    }
  };

  return config;
};

// Setup config/headers and token
export const tokenConfig = getState => {
  const token = getState().auth.token;

  const config = headerConfig();

  // If token, add to heaaders
  if (token) {
    config.headers["x-auth-token"] = token;
  }

  return config;
};
