import Axios from "axios";
import { tokenConfig } from "./authActions";
import {
  GET_DEBTS,
  REGISTER_DEBT,
  SELECT_DEBT,
  DELETE_DEBT,
  GET_PRIORITIZED_DEBTS,
  UPDATE_DEBT,
  GET_DEBTS_TOTAL_VALUE,
  UPDATE_DEBTS_CURRENT_VALUE
} from "./constants";
import { returnErrors } from "./errorActions";

export const selectDebt = debt => {
  return dispatch => {
    console.log("select debt action", debt);

    dispatch({ type: SELECT_DEBT, payload: { selectedDebt: debt } });
  };
};

export const loadDebts = () => {
  return (dispatch, getState) => {
    console.log("To loadando as dívidas");

    Axios.get("/api/debt", tokenConfig(getState))
      .then(res => {
        console.log("res", res);

        dispatch({ type: GET_DEBTS, payload: res.data });
      })
      .catch(err => {
        const { data, status } = err.response;
        dispatch(returnErrors(data, status));
      });
  };
};

export const loadPrioritizedDebts = () => {
  return (dispatch, getState) => {
    console.log("To loadando as dívidas");

    Axios.get("/api/debt/prioritized", tokenConfig(getState))
      .then(res => {
        console.log("res", res);

        dispatch({ type: GET_PRIORITIZED_DEBTS, payload: res.data });
      })
      .catch(err => {
        const { data, status } = err.response;
        dispatch(returnErrors(data, status));
      });
  };
};

export const registerDebt = debtInfo => {
  return (dispatch, getState) => {
    console.log("debtInfo", debtInfo);

    const body = JSON.stringify(debtInfo);

    Axios.post("/api/debt", body, tokenConfig(getState))
      .then(res => dispatch({ type: REGISTER_DEBT }))
      .catch(err => {
        const { data, status } = err.response;
        dispatch(returnErrors(data, status));
      });
  };
};

export const updateDebt = (debtId, updatedDebt) => {
  return (dispatch, getState) => {
    const config = tokenConfig(getState);
    console.log("debtId", debtId);

    console.log("updatedDebt", updatedDebt);

    const body = JSON.stringify({
      name: updatedDebt[0],
      description: updatedDebt[1]
    });

    console.log("delete token config", config);

    console.log("body", body);

    let url = "/api/debt/info/" + debtId;
    Axios.put(url, body, config)
      .then(res => dispatch({ type: UPDATE_DEBT }))
      .catch(err => {
        console.log("err", err);
      });
  };
};

export const deleteDebt = debtId => {
  return (dispatch, getState) => {
    const body = JSON.stringify({ debtId });
    console.log("debtId", debtId);

    const config = tokenConfig(getState);

    console.log("delete token config", config);
    let url = "/api/debt/" + debtId;
    Axios.delete(url, config)
      .then(res => dispatch({ type: DELETE_DEBT }))
      .catch(err => {
        console.log("err", err);
      });
  };
};

export const getDebtsTotalValue = () => {
  return (dispatch, getState) => {
    Axios.get("/api/debt/totalValue", tokenConfig(getState))
      .then(res => {
        console.log("res", res);

        dispatch({ type: GET_DEBTS_TOTAL_VALUE, payload: res.data });
      })
      .catch(err => {
        const { data, status } = err.response;
        dispatch(returnErrors(data, status));
      });
  };
};

export const updateCurrentValue = () => {
  return (dispatch, getState) => {

    const config = tokenConfig(getState);
    let url = "/api/debt/currentValue";
    Axios.put(url, null, config)
      .then(res => dispatch({ type: UPDATE_DEBTS_CURRENT_VALUE }))
      .catch(err => {
        console.log("err", err);
      });
  }
}
