import { GET_DEBTS, SELECT_DEBT, DELETE_DEBT, GET_PRIORITIZED_DEBTS, UPDATE_DEBT, GET_DEBTS_TOTAL_VALUE, UPDATE_DEBTS_CURRENT_VALUE } from "../actions/constants";

const initialState = {
  debts: [],
  highestInterestRate: {},
  expensiveDebt: {},
  prioritizedDebts: [],
  prioritizedHighestInterestRate: {},
  prioritizedExpensiveDebt: {},
  totalValue: 0,
  selectedDebt: null
}

const debtReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_DEBTS:
      return {
        ...state,
        debts: payload.debts,
        highestInterestRate: payload.highestInterestRate,
        expensiveDebt: payload.expensiveDebt,
      }
    case GET_PRIORITIZED_DEBTS:
      return {
        ...state,
        prioritizedDebts: payload.debts,
        prioritizedHighestInterestRate: payload.highestInterestRate,
        prioritizedExpensiveDebt: payload.expensiveDebt,
      }
    case UPDATE_DEBT:
      console.log("debt updated");
       
      return state;
    case GET_DEBTS_TOTAL_VALUE:
      return {
        ...state,
        totalValue: payload.totalValue
      };
    case UPDATE_DEBTS_CURRENT_VALUE:
      return state;
    case SELECT_DEBT:
      return {
        ...state,
        selectedDebt: payload.selectedDebt
      }
    case DELETE_DEBT:
      console.log("deleteeei");
      return state
    default:
      return state;
  }
}

export default debtReducer;