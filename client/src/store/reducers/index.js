import {combineReducers} from 'redux';
import customerReducer from './customer';
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import debtReducer from './debtReducer';

export default combineReducers({
  customers: customerReducer,
  error: errorReducer,
  auth: authReducer,
  debts: debtReducer
})
