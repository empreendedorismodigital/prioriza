const mongoose = require("mongoose");
const { Schema } = mongoose;

const PaymentSchema = new Schema({
  value: { type: Number },
  date: { type: String },
  status: { type: String, default: "aberto" }
});

const DebtSchema = new Schema({
  name: { type: String, required: true, unique: true },
  initialValue: { type: Number, required: true },
  description: { type: String },
  installmentsNumber: {type: Number},
  installmentsValue: {type: Number},
  installments: { type: String },
  dueDate: { type: String, required: true },
  interestRate: { type: Number },
  interestType: { type: String },
  interestAmount: { type: Number },
  currentValue: { type: Number },
  lastUpdate: { type: Date },
  totalOverdue: { type: Number},
  currentValue: { type: Number },
  payments: { type: [PaymentSchema] }
});

mongoose.model('debts', DebtSchema);
