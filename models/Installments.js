const mongoose = require("mongoose");
const { Schema } = mongoose;

const InstallmentsSchema = new Schema({
  array: {type: Array},
});

mongoose.model('installments', InstallmentsSchema);
