const mongoose = require("mongoose");
const User = mongoose.model("users");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("config");
const auth = require("../middleware/auth");
const express = require("express");
const router = express.Router();

// @route   POST api/auth
// @desc    Auth user
// @access  Public
router.post(`/`, async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send({ msg: "Por favor, digite todos os campos" });
  }

  User.findOne({ email }).then(user => {
    if (!user) return res.status(400).send({ msg: "Usuário não existe" });

    // Validate password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (!isMatch) return res.status(400).send({ msg: "Senha inválida" });
      console.log("JWT_SECRET", process.env.JWT_SECRET);
      
      jwt.sign(
        { id: user.id },
        process.env.JWT_SECRET,
        { expiresIn: 3600 },
        (err, token) => {
          if (err) throw err;
          res.send({ token });
        }
      );
    });
  });
});

// @route   GET api/auth/user
// @desc    Get user data
// @access  Private
router.get("/user", auth, (req, res) => {
  User.findById(req.user.id).select('-password').then(user => res.send({user}));
})


module.exports = router;
