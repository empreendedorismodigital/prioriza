const mongoose = require("mongoose");
const Debt = mongoose.model("debts");
const User = mongoose.model("users");
const Installments = mongoose.model("installments");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const config = require("config");

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

const msToDays = timeInMs => {
  return Math.floor(timeInMs / 86400000);
};

const getExpiredMonths = (date, since) => {
  const now = since ? new Date(since) : new Date();
  console.log("since", since);
  console.log("now", now);

  const dueDate = new Date(date);

  let months = (now.getFullYear() - dueDate.getFullYear()) * 12;
  const monthDiff = now.getMonth() - dueDate.getMonth();
  const dayDiff = now.getDate() - dueDate.getDate();

  months += monthDiff;

  if (dayDiff < 0) {
    months -= 1;
  }

  return months < 0 ? 0 : months;
};

const createInstallments = debt => {
  let installmentsArray = [];

  const dueDate = new Date(debt.dueDate + "T00:00:00");
  const now = new Date();

  const dailyInterestRate = ((debt.interestRate / 100) * 12) / 365;
  console.log("dailyInterestRate", dailyInterestRate);

  for (let i = 0; i < debt.installmentsNumber; i++) {
    let monthDueDate, monthCurrentValue;
    let installment = {};

    monthDueDate = new Date(dueDate);
    monthDueDate.setMonth(monthDueDate.getMonth() + i);

    installment.dueDate = monthDueDate;

    installment.number = i + 1;

    const diffDays = msToDays(now - monthDueDate);

    if (diffDays >= 0) {
      installment.status = "overdue";
      console.log("diff days", diffDays);
      monthCurrentValue =
        debt.installmentsValue +
        debt.installmentsValue * (diffDays * dailyInterestRate);

      installment.currentValue = monthCurrentValue;
    } else {
      installment.status = "pending";
      installment.currentValue = debt.installmentsValue;
    }

    installmentsArray.push(installment);
  }

  return installmentsArray;
};

const updateInstallments = async debt => {
  const now = new Date();
  const updatedInstallments = [];
  const dailyInterestRate = ((debt.interestRate / 100) * 12) / 365;

  try {
    const installments = await Installments.findById(debt.installments);

    installments.array.forEach(installment => {
      let updatedInstallment = { ...installment };
      if (installment.status == "overdue") {
        const diffDays = msToDays(now - debt.lastUpdate);
        updatedInstallment.currentValue +=
          updatedInstallment.currentValue * (diffDays * dailyInterestRate);
      } else if (installment.status == "pending") {
        const diffDays = msToDays(now - installment.dueDate);
        console.log("due date", installment.dueDate);
        
        if (diffDays >= 0) {

          updatedInstallment.status = "overdue";
          console.log("updatedInstament", updatedInstallment);
          console.log("diff days", diffDays);
          
          updatedInstallment.currentValue +=
            updatedInstallment.currentValue * (diffDays * dailyInterestRate);
        }
      }

      updatedInstallments.push(updatedInstallment);
    });
  } catch (error) {
    console.log("error", error);
  }

  return updatedInstallments;
};

const calculateInstallmentCurrentValue = async debt => {
  let currentValue = 0;

  try {
    const installments = await Installments.findById(debt.installments);

    installments.array.forEach(installment => {
      if (installment.status == "overdue") {
        currentValue += installment.currentValue;
      }
    });

    console.log("terminei de atualizar os installments");
  } catch (error) {
    console.log("error", error);
  }

  return currentValue;
};

const calculateNormalCurrentValue = debt => {
  let expiredMonths = getExpiredMonths(debt.dueDate + "T00:00:00");

  if (debt.interestType == "Composto") {
    return (
      debt.initialValue * Math.pow(1 + debt.interestRate / 100, expiredMonths)
    );
  } else {
    return debt.initialValue * (1 + expiredMonths * (debt.interestRate / 100));
  }
};

const calculateCurrentValue = debt => {
  if (debt.installmentsNumber) {
    return calculateInstallmentCurrentValue(debt);
  } else {
    return calculateNormalCurrentValue(debt);
  }
};

const updateNormalCurrentValue = debt => {
  let expiredMonths;
  expiredMonths =
    getExpiredMonths(debt.dueDate + "T00:00") -
    getExpiredMonths(debt.dueDate + "T00:00", debt.lastUpdate);

  if (expiredMonths > 0) {
    if (debt.interestType == "Composto") {
      return (
        debt.currentValue * Math.pow(1 + debt.interestRate / 100, expiredMonths)
      );
    } else {
      return (
        debt.currentValue  + debt.initialValue * (expiredMonths * (debt.interestRate / 100))
      );
    }
  }

  return debt.currentValue;
};

const updateInstallmentCurrentValue = debt => {
  return calculateInstallmentCurrentValue(debt);
};

const calculateInterestAmount = debt => {
  console.log("debt do calculate", debt);
  console.log("debt installmentValue do calculate", debt.installmentsValue);

  if (debt.totalOverdue) {
    return debt.currentValue - debt.totalOverdue * debt.installmentsValue;
  }
  ////////////////////////////////////////////////////////////////////////////
  // Adicionar o pagamento aqui;
  ////////////////////////////////////////////////////////////////////////////
  return debt.currentValue - debt.initialValue;
};

// @route   GET api/debt
// @desc    Get user debts
// @access  Private
router.get("/", auth, async (req, res) => {
  console.log("user id", req.user.id);
  User.findById(req.user.id)
    .then(async user => {
      let userDebts = [];
      let expensiveDebt = {
        name: "",
        value: 0
      };
      let highestInterestRate = {
        name: "",
        value: 0
      };

      if (user.debts) {
        try {
          await asyncForEach(user.debts, async debtId => {
            try {
              const debt = await Debt.findById(debtId);
              console.log("debt", debt);

              userDebts.push(debt);

              if(debt.currentValue > expensiveDebt.value){
                expensiveDebt.value = debt.currentValue;
                expensiveDebt.name = debt.name;
              }

              if(debt.interestRate > highestInterestRate.value) {
                highestInterestRate.value = debt.interestRate;
                highestInterestRate.name = debt.name;
              }

            } catch (error) {
              return res.status(400).send("Erro ao buscar dívida");
            }
          });
        } catch (error) {
          return res.status(400).send("Erro ao buscar dívidas");
        }
      }

      console.log("userDebts", userDebts);



      return res.status(200).send({ debts: userDebts, expensiveDebt, highestInterestRate});
    })
    .catch(err => {
      console.log("err", err);

      return res.status(400).send("Erro ao buscar usuário");
    });
});

// @route   GET api/debt
// @desc    Get user debts
// @access  Private
router.get("/totalValue", auth, async (req, res) => {
  console.log("user id", req.user.id);
  User.findById(req.user.id)
    .then(async user => {
      let totalValue = 0;

      if (user.debts) {
        try {
          await asyncForEach(user.debts, async debtId => {
            try {
              const debt = await Debt.findById(debtId);
              console.log("debt", debt);

              totalValue += debt.currentValue;

            } catch (error) {
              return res.status(400).send("Erro ao buscar dívida");
            }
          });
        } catch (error) {
          return res.status(400).send("Erro ao buscar dívidas");
        }
      }

      console.log("totalValue", totalValue);

      return res.status(200).send({ totalValue });
    })
    .catch(err => {
      console.log("err", err);

      return res.status(400).send("Erro ao buscar usuário");
    });
});

// @route   GET api/debt
// @desc    Get user debts
// @access  Private
router.get("/prioritized", auth, async (req, res) => {
  console.log("user id", req.user.id);
  User.findById(req.user.id)
    .then(async user => {
      let userDebts = [];
      let expensiveDebt = {
        name: "",
        value: 0
      };
      let highestInterestRate = {
        name: "",
        value: 0
      };

      if (user.debts) {
        try {
          await asyncForEach(user.debts, async debtId => {
            try {
              const debt = await Debt.findById(debtId);
              console.log("debt", debt);

              userDebts.push(debt);

              if(debt.currentValue > expensiveDebt.value){
                expensiveDebt.value = debt.currentValue;
                expensiveDebt.name = debt.name;
              }

              if(debt.interestRate > highestInterestRate.value) {
                highestInterestRate.value = debt.interestRate;
                highestInterestRate.name = debt.name;
              }

            } catch (error) {
              return res.status(400).send("Erro ao buscar dívida");
            }
          });
        } catch (error) {
          return res.status(400).send("Erro ao buscar dívidas");
        }
      }

      //prioriza
      userDebts.sort((a, b) => (a.interestAmount < b.interestAmount ? 1 : -1));

      console.log("userDebts", userDebts);
      return res.status(200).send({ debts: userDebts, expensiveDebt, highestInterestRate });
    })
    .catch(err => {
      console.log("err", err);

      return res.status(400).send("Erro ao buscar usuário");
    });
});

// @route   GET api/debt/:id
// @desc    Get user debts info
// @access  Private
router.get("/:id", auth, async (req, res) => {
  console.log("user id", req.user.id);
  console.log("debt id", req.params.id);
  let debt = Debt.findById(req.params.id);
  debt
    .then(debt => {
      res.status(200).send(debt);
    })
    .catch(err => {
      res.status(400).send("Erro ao buscar debt info");
    });
});

// @route   POST api/debt
// @desc    Create a debt and associate to a user
// @access  Private
router.post("/", auth, async (req, res) => {
  const { id } = req.user;
  console.log("user id", req.user.id);
  console.log("req body", req.body);

  const debt = new Debt(req.body);

  let totalOverdue;

  if (debt.installmentsNumber) {
    try {
      const installmentsArray = createInstallments(debt);

      totalOverdue = 0;

      installmentsArray.forEach(installment => {
        if (installment.status == "overdue") {
          totalOverdue += 1;
        }
      });

      const installments = new Installments({ array: installmentsArray });
      const createdInstallments = await installments.save();

      console.log("debt criado com sucesso", createdInstallments);

      debt.installments = createdInstallments.id;
    } catch (error) {
      console.log("err", error);

      res.status(400).send("Erro ao associar parcelas à dívida");
    }
  }

  console.log("vou começar o calculate");

  const currentValue = await calculateCurrentValue(debt);

  if (!currentValue) {
    res.status(400).send("Erro ao calcular valor atual da dívida");
    return;
  }

  console.log("currentValue", currentValue);

  debt.currentValue = currentValue;

  debt.totalOverdue = totalOverdue;

  debt.interestAmount = calculateInterestAmount(debt);

  debt.lastUpdate = new Date();

  console.log("debt", debt);

  debt
    .save()
    .then(debt => {
      console.log("debt criado com sucesso", debt);

      User.findById(id)
        .then(user => {
          console.log("user debts", user.debts);
          const newDebts = user.debts ? [...user.debts, debt.id] : [debt._id];
          User.updateOne({ _id: id }, { $set: { debts: newDebts } })
            .then(user => {
              res.status(200).send(user);
            })
            .catch(err => {
              console.log("err", err);

              res.status(400).send("Erro ao associar dívida ao usuário");
            });
        })
        .catch(err => {
          res.status(400).send("Erro buscar usuário");
        });
    })
    .catch(err => {
      console.log("erro", err);
      res.status(400).send("Erro ao criar dívidas");
    });
});

// @route   PUT api/debt/info
// @desc    Update debt info
// @access  Private
router.put(`/info/:debtId`, auth, async (req, res) => {
  const { debtId } = req.params;

  let debt = await Debt.findByIdAndUpdate(debtId, req.body);

  return res.status(202).send({
    error: false,
    debt
  });
});

// @route   PUT api/debt/currentValue
// @desc    Update all debts currentValue
// @access  Private
router.put(`/currentValue`, auth, async (req, res) => {
  console.log("vou atualizar");

  try {
    let user = await User.findById(req.user.id);
    let count = 0;
    await asyncForEach(user.debts, async userDebt => {
      console.log("userDebt", userDebt);
      count += 1;

      try {
        let debt = await Debt.findById(userDebt);

        let currentValue;
        let totalOverdue;

        if (debt.installmentsNumber) {
          console.log("sou parcelado");

          const updatedInstallments = await updateInstallments(debt);

          if (!updateInstallments) {
            return res.status(400).send("Erro ao atualizar valor das parcelas");
          }

          totalOverdue = 0;

          console.log("updated installments", updatedInstallments);

          updatedInstallments.forEach(installment => {
            if (installment.status == "overdue") {
              totalOverdue += 1;
            }
          });

          await Installments.updateOne(
            { _id: debt.installments },
            { $set: { array: updatedInstallments } }
          );

          currentValue = await updateInstallmentCurrentValue(debt);

          if (!currentValue) {
            return res.status(400).send("Erro ao calcular o valor atualizado");
          }
        } else {
          console.log("sou normal");

          currentValue = await updateNormalCurrentValue(debt);
        }

        const updatedDebt = { ...debt._doc, currentValue, totalOverdue };

        const interestAmount = calculateInterestAmount(updatedDebt);

        const lastUpdate = new Date();

        await Debt.updateOne(
          { _id: userDebt },
          { $set: { currentValue, lastUpdate, totalOverdue, interestAmount } }
        );
      } catch (error) {
        console.log("error", error);

        return res
          .status(400)
          .send("Erro ao atualizar valor atual de uma dívida");
      }
    });
    if (count == user.debts.length) {
      return res.status(200).send({});
    } else {
      return res.status(400).send("Não atualizou todas as dívidas");
    }
  } catch (error) {
    return res.status(400).send("Erro ao buscar usuário");
  }
});

// @route   DELETE api/debt
// @desc    Delete debt info
// @access  Private
router.delete(`/:debtId`, auth, async (req, res) => {
  try {
    console.log("chamei o delete");

    const { debtId } = req.params;

    let user = await User.findById(req.user.id);

    user.debts = user.debts.filter(value => {
      return value != debtId;
    });

    let userWithRemovedDebt = await User.updateOne(
      { _id: req.user.id },
      { $set: { debts: user.debts } }
    );

    let debt = await Debt.findById(debtId);
    
    if (debt.installmentsNumber) {
      await Installments.deleteOne({_id: debt.installments}); 
    }


    await Debt.deleteOne({ _id: debtId });

    return res.status(202).send({
      error: false
    });
  } catch (error) {
    console.log("erro", error);
    
    res.status(400).send("Erro ao excluir dívidas");
  }
});

module.exports = router;
