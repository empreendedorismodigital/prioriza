const mongoose = require("mongoose");
const User = mongoose.model("users");
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const jwt = require("jsonwebtoken");
const config = require("config");

router.get(`/`, async (req, res) => {
  let users = await User.find();
  return res.status(200).send(users);
});

router.post(`/`, async (req, res) => {
  const { email, password, cpf, name } = req.body;
  console.log("req body", req.body);
  
  const user = new User({ email, password, cpf, name });
  user
    .save()
    .then(user => {
      console.log(" to entrando aqui");
      
      jwt.sign(
        { id: user.id },
        process.env.JWT_SECRET,
        { expiresIn: 3600 },
        (err, token) => {
          if (err) throw err;
          res.send({ token });
        }
      );
    })
    .catch(err => {
      console.log("erro 2", err);

      res
        .status(400)
        .send("Erro ao cadastrar novo usuário, por favor tente novamente.");
    });
});

router.put(`/`, auth, async (req, res) => {

  let user = await User.update({"_id": req.user.id}, req.body);

  return res.status(202).send({
    error: false,
    user
  });
});

router.delete(`/`, async (req, res) => {

  let user = await User.findByIdAndDelete(req.user.id);

  return res.status(202).send({
    error: false,
    user
  });
});

module.exports = router;
