const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

require('dotenv').config()

// import models
require('./models/User');
require('./models/Debt');
require('./models/Installments');

const app = express();

// DB config
const db = process.env.MONGO_URI

mongoose.Promise = global.Promise;

// Connect to Mongo
mongoose.connect(db, 
                  {useUnifiedTopology: true, useNewUrlParser: true}
                ).then(() => console.log('DB Connected!'))
                .catch(err => {console.log(`DB Connection Error: ${err.message}`);
});
mongoose.set('useCreateIndex', true);

app.use(bodyParser.json());

//import routes

app.use('/api/user', require('./routes/userRoute'));

app.use('/api/auth', require('./routes/authRoute'));

app.use('/api/debt', require('./routes/debtRoute'));

require('./routes/costumerRoute')(app);

// Serve static assets if in production
if(process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(
`Server running on port ${PORT}`));
